package com.example.ronald.actividadcalificada01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FormPropietarioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_propietario);


    }



    public void regcochera(View v){

        Intent intentRegCochera = new Intent(this, FormRegistrarEstacionamiento.class);
        startActivity(intentRegCochera);
    }


    public void historico(View v){

        Intent intentHistorico = new Intent(this, HistoricoAlquilerActivity.class);
        startActivity(intentHistorico);
    }

}
