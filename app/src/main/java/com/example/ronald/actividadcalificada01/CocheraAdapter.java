package com.example.ronald.actividadcalificada01;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;


public class CocheraAdapter extends RecyclerView.Adapter<CocheraAdapter.MyViewHolder> {

    private List<Cochera> cocherasList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView codigo, nombre, tipoEstacionamiento, tipoPropiedad, direccion, tarifa;

        public MyViewHolder(View view) {
            super(view);
            direccion = (TextView) view.findViewById(R.id.direccion);
            tipoEstacionamiento = (TextView) view.findViewById(R.id.tipoEstacionamiento);
            tipoPropiedad = (TextView) view.findViewById(R.id.tipoPropiedad);
            tarifa = (TextView) view.findViewById(R.id.tarifa);
        }
    }

    public CocheraAdapter(List<Cochera> cocherasList) {
        this.cocherasList = cocherasList;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cochera_fila, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Cochera cochera = cocherasList.get(position);
        holder.direccion.setText(cochera.getDireccion());
        holder.tipoEstacionamiento.setText(cochera.getTipoEstacionamiento());
        holder.tipoPropiedad.setText(cochera.getTipoPropiedad());
        holder.tarifa.setText(cochera.getTarifa());
    }

    @Override
    public int getItemCount() {
        return cocherasList.size();
    }

}
