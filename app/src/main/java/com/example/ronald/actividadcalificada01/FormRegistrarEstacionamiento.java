package com.example.ronald.actividadcalificada01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class FormRegistrarEstacionamiento extends AppCompatActivity implements GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener,
        OnMapReadyCallback {


    String valorSeleccionadoTipoEstacionamiento;
    String valorSeleccionadoTipoPropiedad;
    String latitud;
    String longitud;

    EditText txtNombre;
    EditText txtDireccion;
    EditText txtTarifa;

    CheckBox chkLunes;
    CheckBox chkMartes;
    CheckBox chkMiercoles;
    CheckBox chkJueves;
    CheckBox chkViernes;
    CheckBox chkSabado;
    CheckBox chkDomingo;

    SupportMapFragment mapFragment;

    private GoogleMap mMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_registrar_estacionamiento);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        final Spinner spinnerTipoEstacionamiento = (Spinner) findViewById(R.id.spinner); //el id del componente en el Activity
        final Spinner spinnerTipoPropiedad = (Spinner) findViewById(R.id.spinner2); //el id del componente en el Activity

         txtNombre = (EditText)findViewById(R.id.txtNombreEstacionamiento);
         txtDireccion = (EditText)findViewById(R.id.txtDireccion);
         txtTarifa = (EditText)findViewById(R.id.txtTarifa);

         chkLunes = (CheckBox)findViewById(R.id.chkLunes);
         chkMartes = (CheckBox)findViewById(R.id.chkMartes);
         chkMiercoles = (CheckBox)findViewById(R.id.chkMiercoles);
         chkJueves = (CheckBox)findViewById(R.id.chkJueves);
         chkViernes = (CheckBox)findViewById(R.id.chkViernes);
         chkSabado = (CheckBox)findViewById(R.id.chkSabado);
         chkDomingo = (CheckBox)findViewById(R.id.chkDomingo);

        //ArrayAnios : es el nombre del string-array en anios.xml

        ArrayAdapter<String> adaptadorTipoEstacionamiento = new ArrayAdapter<String>(this,    android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.ArrayTipoEstacionamiento));
        adaptadorTipoEstacionamiento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoEstacionamiento.setAdapter(adaptadorTipoEstacionamiento);


        ArrayList<String>  objectTipoPropiedad = getTipoPropiedad();
        ArrayAdapter<String> adaptadorTipoPropiedad = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,  getResources().getStringArray(R.array.ArrayTipoPropiedad));
        adaptadorTipoPropiedad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoPropiedad.setAdapter(adaptadorTipoPropiedad);

        spinnerTipoEstacionamiento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                valorSeleccionadoTipoEstacionamiento = parent.getSelectedItem().toString();
            }

            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(FormRegistrarEstacionamiento.this, "No ha seleccionado",    Toast.LENGTH_LONG).show();
            }
        });


        spinnerTipoPropiedad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                valorSeleccionadoTipoPropiedad = parent.getSelectedItem().toString();
            }

            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(FormRegistrarEstacionamiento.this, "No ha seleccionado",    Toast.LENGTH_LONG).show();
            }
        });


    }


    public ArrayList<String>  getTipoCochera(){
        final ArrayList<String> listaTipoCochera = new ArrayList<String>();
        String url = "http://demopagesupc.atwebpages.com/index_cochera.php/tiposcochera";

        StringRequest stringRequest= new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.i("======>", jsonArray.toString());

                    for (int i=0; i<jsonArray.length(); i++){
                        JSONObject object = jsonArray.getJSONObject(i);
                        listaTipoCochera.add(object.getString("descripcion"));
                    }
                } catch (JSONException e) {
                    Log.i("======>", e.getMessage());
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("======>", error.toString());
                    }
                }
        );
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        return listaTipoCochera;
    }

    public ArrayList<String>  getTipoPropiedad(){
        final ArrayList<String> listaPropiedad = new ArrayList<String>();
        String url = "http://demopagesupc.atwebpages.com/index_cochera.php/tiposcocheraprop";

        StringRequest stringRequest= new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.i("======>", jsonArray.toString());

                    for (int i=0; i<jsonArray.length(); i++){
                        JSONObject object = jsonArray.getJSONObject(i);
                        listaPropiedad.add(object.getString("descripcion"));
                    }
                } catch (JSONException e) {
                    Log.i("======>", e.getMessage());
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("======>", error.toString());
                    }
                }
        );
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        return listaPropiedad;
    }

    public void RegistrarEstacionamiento(View view){
        Button button = (Button) view;
        int idButton = button.getId();
        if(idButton == R.id.btnRegistrar) {

            if( (latitud == null && longitud == null) || (latitud.equals("") && longitud.equals(""))){
                Toast toast = Toast.makeText(FormRegistrarEstacionamiento.this,"Debe seleccionar un punto en el mapa", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            else {
                String url = "http://demopagesupc.atwebpages.com/index_cochera.php/cochera";

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast toast = Toast.makeText(FormRegistrarEstacionamiento.this, "La cochera se registro correctamente!", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.i("======>", error.toString());
                                Toast toast = Toast.makeText(FormRegistrarEstacionamiento.this, "Error al registrar la cochera!", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        }) {

                    @Override
                    protected Map<String, String> getParams() {

                        Map<String, String> params = new HashMap();
                        params.put("nombre", txtNombre.getText().toString());
                        params.put("tamanio", obtenerTipoEstacionamiento(valorSeleccionadoTipoEstacionamiento));
                        params.put("tipo", obtenerTipoPropiedad(valorSeleccionadoTipoPropiedad));
                        params.put("direccion", txtDireccion.getText().toString());
                        params.put("tarifa", txtTarifa.getText().toString());
                        params.put("idusuario", "1");
                        params.put("coordenada_x", latitud);
                        params.put("coordenada_y", longitud);
                        params.put("dia_lunes", chkLunes.isChecked() ? "1" : "0");
                        params.put("dia_martes", chkMartes.isChecked() ? "1" : "0");
                        params.put("dia_miercoles", chkMiercoles.isChecked() ? "1" : "0");
                        params.put("dia_jueves", chkJueves.isChecked() ? "1" : "0");
                        params.put("dia_viernes", chkViernes.isChecked() ? "1" : "0");
                        params.put("dia_sabado", chkSabado.isChecked() ? "1" : "0");
                        params.put("dia_domingo", chkDomingo.isChecked() ? "1" : "0");
                        return params;
                    }


                };

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }
        }

        }

        String obtenerTipoEstacionamiento(String tipoEstacionamiento){

            if(tipoEstacionamiento.equals("Pequeño")){
                return "1";
            }
            else if(tipoEstacionamiento.equals("Mediano")){
                return "2";
            }
            else if(tipoEstacionamiento.equals("Grande")){
                return "3";
            }
            return "";
        }

        String obtenerTipoPropiedad(String tipoPropiedad){

            if(tipoPropiedad.equals("Casa propia")){
                return "1";
            }
            else if(tipoPropiedad.equals("Casa alquilada")){
                return "2";
            }
            else if(tipoPropiedad.equals("Casa compartida")){
                return "3";
            }
            return "";
        }

    @Override
    public void onMapClick(LatLng latLng) {

        mMap.clear();
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latLng.latitude, latLng.longitude))
                .title("Mi posicion")).showInfoWindow();

        latitud = latLng.latitude + "";
        longitud = latLng.longitude + "";
        System.out.println("llllllllllllllllll : " + latitud  + " - " + longitud);
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        System.out.println(latLng);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setTrafficEnabled(true);
/*
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(-12.04592, -77.030565))
                .title("Centro de Lima")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));*/
/*
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(-12.044956, -77.029831))
                .title("Palacio de Gobierno"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(-12.046661, -77.029544))
                .title("Catedral"));
*/
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-12.04592, -77.030565), 15));

        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setTrafficEnabled(true);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        googleMap.setOnMapClickListener(this);
        googleMap.setOnMapLongClickListener(this);
    }
}
