package com.example.ronald.actividadcalificada01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class FormRegistroAutoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_registro_auto);



        Spinner spinnerYear = (Spinner) findViewById(R.id.spinner); //el id del componente en el Activity

        //ArrayAnios : es el nombre del string-array en anios.xml

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,    android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.ArrayAnios));
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerYear.setAdapter(adaptador);

        spinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String selectedYear = parent.getSelectedItem().toString();
                Toast.makeText(FormRegistroAutoActivity.this, "Seleccionado: " + selectedYear, Toast.LENGTH_LONG).show();
            }

            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(FormRegistroAutoActivity.this, "No ha seleccionado",    Toast.LENGTH_LONG).show();
            }
        });


    }

    public void Grabar(View v){
        final EditText txtPlaca = findViewById(R.id.editText8);
        final EditText txtMarca = findViewById(R.id.editText1);
        final EditText txtModelo = findViewById(R.id.txtEmail);

        String url = "http://demopagesupc.atwebpages.com/index_cochera.php/vehiculo";

        StringRequest stringRequest= new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast toast = Toast.makeText(FormRegistroAutoActivity.this,"Se insertó correctamente", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("======>", error.toString());
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() {
                Spinner spinner = (Spinner)findViewById(R.id.spinner);
                String anio = spinner.getSelectedItem().toString();

                Map<String, String> params = new HashMap();
                params.put("placa", txtPlaca.getText().toString().toUpperCase());
                params.put("marca", txtMarca.getText().toString().toUpperCase());
                params.put("modelo", txtModelo.getText().toString().toUpperCase());
                params.put("anio", anio);
                params.put("idusuario", "1");
                return params;
            }
        };

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}
