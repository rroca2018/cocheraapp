package com.example.ronald.actividadcalificada01;

public class Cochera {

    private int codigo;
    private String nombre;
    private String tipoEstacionamiento;
    private String tipoPropiedad;
    private String direccion;
    private String tarifa;

    public Cochera(int codigo, String nombre, String tipoEstacionamiento, String tipoPropiedad, String direccion, String tarifa) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.tipoEstacionamiento = tipoEstacionamiento;
        this.tipoPropiedad = tipoPropiedad;
        this.direccion = direccion;
        this.tarifa = tarifa;
    }


    public Cochera(String tipoEstacionamiento, String tipoPropiedad, String direccion, String tarifa) {
        this.tipoEstacionamiento = tipoEstacionamiento;
        this.tipoPropiedad = tipoPropiedad;
        this.direccion = direccion;
        this.tarifa = tarifa;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoEstacionamiento() {
        return tipoEstacionamiento;
    }

    public void setTipoEstacionamiento(String tipoEstacionamiento) {
        this.tipoEstacionamiento = tipoEstacionamiento;
    }

    public String getTipoPropiedad() {
        return tipoPropiedad;
    }

    public void setTipoPropiedad(String tipoPropiedad) {
        this.tipoPropiedad = tipoPropiedad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTarifa() {
        return tarifa;
    }

    public void setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }
}
