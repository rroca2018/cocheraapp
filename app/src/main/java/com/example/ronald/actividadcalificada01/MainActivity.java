package com.example.ronald.actividadcalificada01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private RadioButton rdPropietario, rdInteresado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = findViewById(R.id.textViewRegistrar);
        textView.setOnClickListener(this);

        Button btnIngresar = findViewById(R.id.btnIngresar);
        btnIngresar.setOnClickListener(this);

        rdPropietario =  findViewById(R.id.radioButton3);
        rdInteresado =  findViewById(R.id.radioButton4);

        rdPropietario.setChecked(true);
    }

    @Override
    public void onClick(View view) {
        boolean valor = false;
        TextView text = (TextView) view;
        int id  = text.getId();

        if(id == R.id.textViewRegistrar){
            valor = true;
            Intent intentRegistroNuevo = new Intent(this, RegistrarNuevoUsuario.class);
            startActivity(intentRegistroNuevo);
        }
        if(valor == false) {
            Button button = (Button) view;
            int idButton = button.getId();
            if(idButton == R.id.btnIngresar){
                this.validarUsuario();
             /*   if(rdPropietario.isChecked()){
                    //Intent intentRegistrarEstacionamiento = new Intent(this, FormRegistrarEstacionamiento.class);
                    //startActivity(intentRegistrarEstacionamiento);

                    Intent intentFormPropietario = new Intent(this, FormPropietarioActivity.class);
                    startActivity(intentFormPropietario);
                }else if(rdInteresado.isChecked()){
                    Intent intentRegistrarAuto = new Intent(this, BotonesActivity.class);
                    startActivity(intentRegistrarAuto);
                }*/

            }
        }

    }


    public void validarUsuario(){
        final boolean validacionCorrecta;
        final EditText txtUser = findViewById(R.id.txtUser);
        final EditText txtPassword = findViewById(R.id.txtPassword);


        String url = "http://demopagesupc.atwebpages.com/index_cochera.php/acceso";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("======>", response);

                        try{
                        JSONArray jsonArray = new JSONArray(response);
                        Log.i("======>", jsonArray.toString());
                        if(jsonArray.length()>0){

                            Intent intentFormPropietario=null;
                            if(rdPropietario.isChecked()){
                                intentFormPropietario = new Intent(getApplicationContext(), FormPropietarioActivity.class);
                            }else{
                                intentFormPropietario = new Intent(getApplicationContext(), BotonesActivity.class);
                            }
                            startActivity(intentFormPropietario);
                        }else{
                            Toast toast = Toast.makeText(MainActivity.this,"El usuario o password incorrectos!", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                       /* for (int i=0; i<jsonArray.length(); i++){
                            JSONObject object = jsonArray.getJSONObject(i);
                           // listaTipoCochera.add(object.getString("idusuario"));
                        }*/

                        } catch (JSONException e) {
                            Log.i("======>", e.getMessage());
                        }


                        /*Intent intentRegistroNuevo = new Intent(null, MainActivity.class);
                        startActivity(intentRegistroNuevo);*/
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("======>", error.toString());
                    }
                }){

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap();
                params.put("usuario", txtUser.getText().toString());
                params.put("clave", txtPassword.getText().toString());

                return params;
            }

        };

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
