package com.example.ronald.actividadcalificada01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


public class FormListarCochera extends AppCompatActivity {

    private List<Cochera> cocheraList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CocheraAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_listar_cochera);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this,3));

        mAdapter = new CocheraAdapter(cocheraList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));


        recyclerView.setAdapter(mAdapter);

        preparePeliculaData();
    }


    private void preparePeliculaData() {
        Cochera cochera1 = new Cochera("Pequeño", "Casa propia", "jr celendin 292" , "20.00");
        cocheraList.add(cochera1);

        Cochera cochera2 = new Cochera("Mediano", "Casa alquilada", "jr chincha 111" , "25.00");
        cocheraList.add(cochera2);

        Cochera cochera3 = new Cochera("Grande", "Casa compartida", "jr progreso 4543" , "30.00");
        cocheraList.add(cochera3);
    }
}
