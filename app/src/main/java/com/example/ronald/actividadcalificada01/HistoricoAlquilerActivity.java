package com.example.ronald.actividadcalificada01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HistoricoAlquilerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico_alquiler);


        String url = "http://demopagesupc.atwebpages.com/index_cochera.php/historicocochera/1";

        StringRequest stringRequest= new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.i("======>", jsonArray.toString());

                    String[] from = new String[]{"Fecha","Placa","Monto"};
                    int[] to = new int[]{R.id.dia,R.id.placa,R.id.monto};

                    ArrayList<HashMap<String,String>> items = new ArrayList<HashMap<String, String>>();

                    //List<String> items = new ArrayList<>();
                    for (int i=0; i<jsonArray.length(); i++){
                        HashMap<String, String> datosEvento = new HashMap<String, String>();
                        JSONObject object = jsonArray.getJSONObject(i);
                        datosEvento.put("Fecha",object.getString("fecha"));
                        datosEvento.put("Placa",object.getString("placa").toUpperCase());
                        datosEvento.put("Monto","S/."+object.getString("pago"));
                        items.add(datosEvento);

                    }


                    SimpleAdapter adaptador = new SimpleAdapter(
                            HistoricoAlquilerActivity.this,items,
                            R.layout.activity_historico_fila,
                            from,to);
                    ListView lv = (ListView) findViewById(R.id.lista1);
                    lv.setAdapter(adaptador);

                } catch (JSONException e) {
                    Log.i("======>", e.getMessage());
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("======>", error.toString());
                    }
                }
        );
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }



}
