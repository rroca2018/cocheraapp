package com.example.ronald.actividadcalificada01;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class RegistrarNuevoUsuario extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_nuevo_usuario);
    }

    public void registrarUsuario(View view){
        final EditText txtUsuario = findViewById(R.id.txtUsuario);
        final EditText txtEmail = findViewById(R.id.txtEmail);
        final EditText txtTelefono = findViewById(R.id.txtTelefono);
        final EditText txtClave = findViewById(R.id.txtClave);
        EditText txtRepetirClave = findViewById(R.id.txtRepetirClave);

        String url = "http://demopagesupc.atwebpages.com/index_cochera.php/usuario";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast toast = Toast.makeText(RegistrarNuevoUsuario.this,"El usuario se registro correctamente!", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        /*Intent intentRegistroNuevo = new Intent(null, MainActivity.class);
                        startActivity(intentRegistroNuevo);*/
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("======>", error.toString());
                    }
                }){

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap();
                params.put("usuario", txtUsuario.getText().toString());
                params.put("clave", txtClave.getText().toString());
                params.put("nombre", "SISTEMA COCHERA");
                params.put("correo", txtEmail.getText().toString());
                params.put("telefono", txtTelefono.getText().toString());
                params.put("flag_propietario","1");
                return params;
            }

        };

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
