package com.example.ronald.actividadcalificada01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BotonesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_botones);

    }

    public void listar(View v){

        Intent intentListar = new Intent(this, FormListarCochera.class);
        startActivity(intentListar);
    }


    public void rentar(View v){

        Intent intentRentar = new Intent(this, AlquilerCocheraActivity.class);
        startActivity(intentRentar);
    }

    public void pagar(View v){

        Intent intentPagar = new Intent(this, FormPagoActivity.class);
        startActivity(intentPagar);
    }

    public void vehiculo(View v){

        Intent intentRegVehiculo = new Intent(this, FormRegistroAutoActivity.class);
        startActivity(intentRegVehiculo);
    }

}
